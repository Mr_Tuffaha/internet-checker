#!/bin/bash

# things to change
pinged_cmd="echo pinged"
not_pinged_cmd="echo not pinged"
not_connect_cmd="echo not connected"
ping_destination="1.1.1.1"

# please keep the rest as is unless you know what you are doing

while [ true ]; do
    ping $ping_destination -W 2 -c 1 &> /tmp/ping_result
    ping_result=$(cat /tmp/ping_result)
    if [ $(echo $ping_result | grep -c "1 received") -eq 1 ]; then
        $(echo $pinged_cmd)
    elif [ $(echo $ping_result | grep -c "Network is unreachable") -eq 1 ]; then
        $(echo $not_connect_cmd)
    else
        $(echo $not_pinged_cmd)
    fi
    sleep 1
done